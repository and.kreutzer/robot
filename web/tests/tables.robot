*** Settings ***
Resource     base.robot

Test Setup         Nova sessão  
# Comportamento que será executado ANTES de cada caso de teste com a utilização de keywords

Test Teardown      Encerra sessão
# Comportamento que será executado APÓS cada caso de teste com a utilização de keywords

*** Test Cases ***
Verificar o valor ao informar o numero da linha
    Go To                               ${url}/tables
    Table Row Should Contain            id:actors   2   @vindiesel

Descobre a linha pelo texto e valido os demais valores
    Go To               ${url}/tables
    ${target}=          Get WebElement      xpath:.//tr[contains(., '@chadwickboseman')]
    Log                 ${target.text}
    Log To Console      ${target.text}  
    Should Contain      ${target.text}      $ 700.00
    Should Contain      ${target.text}      Pantera Negra    

# Evitar utilizar o full Xpath pronto
# 1- Selecionar o componente desejado (table) e dar ctrl+f;
# 2- Colocar .// e buscar um elemento de referência (tr) - verificar porque é utilizado .// (http://www.macoratti.net/vb_xpath.htm)
# 3- Filtrar com a busca por include ou contains com parâmetros únicos do componente desejado - ex.: [contains(.,'@robertdowneyjr')]
# 4- Exemplo para o caso em questão: Xpath - .//tr[contains(.,'@vindiesel')] para a linha 2 da tabela
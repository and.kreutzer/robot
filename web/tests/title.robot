*** Settings ***
Resource        base.robot

Test Setup         Nova sessão  
# Comportamento que será executado ANTES de cada caso de teste com a utilização de keywords

Test Teardown      Encerra sessão
# Comportamento que será executado APÓS cada caso de teste com a utilização de keywords

*** Test Cases ***
Should See Page Title
    Title Should Be     Training Wheels Protocol

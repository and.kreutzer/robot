*** Settings ***
Resource     base.robot

Test Setup         Nova sessão  
# Comportamento que será executado ANTES de cada caso de teste com a utilização de keywords

Test Teardown      Encerra sessão
# Comportamento que será executado APÓS cada caso de teste com a utilização de keywords

*** Test Cases ***
Selecionando por Id
    Go to                           ${url}/radios
    Select Radio Button             movies      cap
#Para utilização da Select Radio Button os parametros são: grupo e id
    Radio Button Should Be Set To   movies      cap
#Verifica se o botão foi selecionado

Selecionando por Value
    Go to                           ${url}/radios
    Select Radio Button             movies      guardians
#Para utilização da Select Radio Button os parametros são: grupo e id
    Radio Button Should Be Set To   movies      guardians
#Verifica se o botão foi selecionado

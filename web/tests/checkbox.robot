*** Settings ***
Resource     base.robot

Test Setup         Nova sessão  
# Comportamento que será executado ANTES de cada caso de teste com a utilização de keywords

Test Teardown      Encerra sessão
# Comportamento que será executado APÓS cada caso de teste com a utilização de keywords

*** Variables ***
${check_thor}       id:thor
${check_iron}       css:input[value='iron-man']
${check_panther}    xpath://*[@id='checkboxes']/input[7]

***Test Cases ***
Marcando opção com Id
# Usar robot -d ./log checkbox.robot / -d ./(nome da pasta) é usado para criar uma nova pasta onde os arquivos de resultado serão salvos
    Go to                           ${url}/checkboxes
    Select Checkbox                 ${check_thor} 
    Checkbox Should Be Selected     ${check_thor} 

Marcando opção com CSS Selector
# Usar robot -d ./log -i ironman checkbox.robot / -i (nome da tag) é usado para executar somente casos com essa tag
    [tags]  checkbox_ironman
    Go to                           ${url}/checkboxes
    Select Checkbox                 ${check_iron} 
    Checkbox Should Be Selected     ${check_iron} 
#    Sleep                           5

Marcando opção com Xpath
    [tags]  panther
    Go to                           ${url}/checkboxes
    Select Checkbox                 ${check_panther}
    Checkbox Should Be Selected     ${check_panther}

Marcando opção com fail
    [tags]  checkbox_fail
    Go to                           ${url}/checkboxes
    Select Checkbox                 id:thor2
    Checkbox Should Be Selected     id:thor2
#    Sleep                           5
